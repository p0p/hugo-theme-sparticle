var nightmodeswitch  = document.querySelector('input[name=nightmode]');



function setNightMode(mode, classoff, classon) {
    //I would use classList.replace but safari doesn't support it
    document.getElementsByTagName('html')[0].classList.remove(classoff);
    document.getElementsByTagName('html')[0].classList.add(classon);
    localStorage.setItem("nightmode", mode);
}

function getNightMode() {
    var nightmode = localStorage.getItem("nightmode");
    if( nightmode == "on" ) {
	nightmodeswitch.checked = true;
	setNightMode('on', 'light', 'dark');
    }
    document.getElementById('togglenightmode').style.display = "block";
}

nightmodeswitch.addEventListener('change', function() {
  if(this.checked) {
      setNightMode('on', 'light', 'dark');
  } else {
      setNightMode('off', 'dark', 'light');
  }
})

window.onload = getNightMode();
    
